package api

interface ApiInterface {
    fun sendMessage(chatId: String, message: String)
}