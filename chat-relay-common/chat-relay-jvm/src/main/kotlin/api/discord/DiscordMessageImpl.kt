package api.discord

import api.Message
import api.SOURCE_DISCORD

data class DiscordMessageImpl(
        override var author: String,
        override var attachments: MutableSet<String>,
        override var date: Long,
        override var text: String
) : Message {
    override var source = SOURCE_DISCORD
}