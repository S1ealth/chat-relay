package api.discord

import api.ApiInterface
import api.addToQueue
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.JDABuilder
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.EventListener

class DiscordApiImpl : ApiInterface {

    private var bot: JDA? = null

    init {
        this.bot = JDABuilder()
                .setToken("NTA2MDQ2MTUwMzE5ODAwMzMx.DuGwbQ.MvO-O1GWEt4ucx8cn66-AIEV_T8")
                .addEventListener(EventListener { event -> if (event is MessageReceivedEvent) onMessageReceived(event) })
                .build()
                .awaitReady()
    }

    override fun sendMessage(chatId: String, message: String) {
        val guild = this.bot?.guildCache?.filter { it.textChannelCache?.getElementById(chatId) !== null }
        guild?.map { it.getTextChannelById(chatId).sendMessage(message).complete() }
    }

    private fun onMessageReceived(event: MessageReceivedEvent) {
        val message = event.message
        if (message.author.id == this.bot?.selfUser?.id) return
        addToQueue(
                DiscordMessageImpl(
                        message.author.name,
                        message.attachments.map { it.url }.toMutableSet(),
                        message.creationTime.toEpochSecond(),
                        message.contentRaw
                )
        )
    }

}