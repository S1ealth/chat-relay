package api

interface Message {
    val author: String
    val attachments: MutableSet<String>
    val date: Long
    val source: Int
    val text: String
}