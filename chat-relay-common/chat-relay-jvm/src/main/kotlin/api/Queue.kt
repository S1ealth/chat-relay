package api

import api.discord.DiscordApiImpl
import api.telegram.TelegramApiImpl

const val SOURCE_TELEGRAM = 1
const val SOURCE_DISCORD = 2

val messengers = mapOf(
        SOURCE_DISCORD to DiscordApiImpl(),
        SOURCE_TELEGRAM to TelegramApiImpl()
)

val queue = mutableListOf<Message>()

fun addToQueue(message: Message){
    queue.add(message)
}
