package api.telegram

import api.ApiInterface
import api.addToQueue
import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.UpdatesListener
import com.pengrad.telegrambot.request.SendMessage

class TelegramApiImpl : ApiInterface {

    private var bot: TelegramBot? = null

    init {
        this.bot = TelegramBot("771106311:AAH956WRNco6wH4WrsNUszWE_8PaufCJy8o")
        this.bot?.setUpdatesListener { updates ->
            updates?.forEach {
                val attachments = when (it.message().photo()){
                    null -> mutableSetOf<String>()
                    else -> it.message().photo().map { photo -> photo.fileId() }.toMutableSet()
                }

                addToQueue(
                        TelegramMessageImpl(
                                it.message().authorSignature() ?: "none",
                                attachments,
                                it.message().date().toLong(),
                                it.message().text() ?: "empty_text"
                        )
                )
            }
            UpdatesListener.CONFIRMED_UPDATES_ALL
        }
    }

    override fun sendMessage(chatId: String, message: String) {
        val request = SendMessage(chatId, message)
        this.bot?.execute(request)
    }

}