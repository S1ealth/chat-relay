package api.telegram

import api.Message
import api.SOURCE_TELEGRAM

data class TelegramMessageImpl(
        override var author: String,
        override var attachments: MutableSet<String>,
        override var date: Long,
        override var text: String
) : Message {
    override var source = SOURCE_TELEGRAM
}