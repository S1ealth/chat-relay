import api.Message
import api.queue
import kotlinx.coroutines.runBlocking
import kotlin.concurrent.timer

@Volatile var keepProcessing = true

fun main(args: Array<String>) {
    processQuery(keepProcessing)
}

fun processQuery(keepProcessing: Boolean) = runBlocking {
    timer(name= "queueCheck" , daemon = false, initialDelay = 10000, period = 1000) {
        if (!queue.isEmpty()) {
            processMessage(queue[0])
            queue.removeAt(0)
        }
    }
}

fun processMessage(message: Message) {
    println(message.text)
}